using UnityEngine;

public class ObjectManager : MonoBehaviour
{
    [SerializeField] private GameObject objectPrefab;

    public Vector3 fuelPos;

    private void Awake()
    {
        var rangeX = Random.Range(-100, 100);
        var rangeY = Random.Range(-100, 100);
        GameObject obj = Instantiate(objectPrefab, new Vector3(rangeX, rangeY, 0), Quaternion.identity);
        fuelPos = obj.transform.position;
    }
}
