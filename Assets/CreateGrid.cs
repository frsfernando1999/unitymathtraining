using UnityEngine;

public class CreateGrid : MonoBehaviour
{
    [SerializeField] private int size = 20;
    private Coords _gridStart = new Coords(-160, -100, -1);

    private void Start()
    {
        for (int x = -160; x <= 160; x+= size)
        {
            if (x == 0) { continue; }
            Coords startPoint = new Coords(x, _gridStart.Y, -1);
            Coords endPoint = new Coords(x, -1 * _gridStart.Y, -1);
            Coords.DrawLine(startPoint, endPoint, 0.5f, Color.gray);
        }
        
        for (int y = -100; y <= 100; y += size)
        {
            if (y == 0) continue; 
            Coords startPoint = new Coords(_gridStart.X, y, -1);
            Coords endPoint = new Coords(-1 * _gridStart.X, y, -1);
            Coords.DrawLine(startPoint, endPoint, 0.5f, Color.gray);
        }
    }
}