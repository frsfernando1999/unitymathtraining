using System;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    public GameObject tank;
    public ObjectManager fuel;
    
    public TextMeshProUGUI tankPos;
    public TextMeshProUGUI fuelPos;
    public TextMeshProUGUI energy;


    private void Start()
    {
        fuelPos.text = fuel.fuelPos.ToString();
    }

    private void Update()
    {
        tankPos.text = tank.transform.position.ToString();
    }
}
