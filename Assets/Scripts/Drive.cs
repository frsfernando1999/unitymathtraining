﻿using System.Collections;
using UnityEngine;
using TMPro;

// A very simplistic car driving on the x-z plane.

public class Drive : MonoBehaviour
{
    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    public float extraFuelPercent = 50f;
    public float energyDepletionRate = 1f;
    public float transitionTime = 5f;
    public float transitionTurnTime = 3f;
    public TextMeshProUGUI energyText;
    public TextMeshProUGUI distanceText;

    private float _energy;
    private Coords _fuelLocation;

    private Coords _normal;

    private void Start()
    {
        _fuelLocation = new Coords(GameObject.FindGameObjectWithTag("Fuel").transform.position);
        SetUIText();

        var position = transform.position;

        _normal = Math.GetNormal(new Coords(_fuelLocation.ToVector() - position));
    }

    private void SetUIText()
    {
        var distance = Math.Distance(new Coords(transform.position), _fuelLocation);
        _energy = distance + extraFuelPercent / 100 * distance;
        distanceText.text = Mathf.CeilToInt(distance).ToString();
    }

    private void Update2()
    {
        var distance = Math.Distance(new Coords(transform.position), _fuelLocation);
        if (distance <= 1f) return;
        transform.position += _normal.ToVector() * (Time.deltaTime * speed);
    }

    public void RotateTank(string rotationAmount)
    {
        if (float.TryParse(rotationAmount, out var angle))
        {
            transform.up = Math.RotateByAngle(angle, new Coords(transform.up)).ToVector();
        }
    }

    void Update()
    {
        if (_energy <= 0) return;
        distanceText.text = Mathf.CeilToInt(Math.Distance(new Coords(transform.position), _fuelLocation)).ToString();

        // Get the horizontal and vertical axis.
        // By default they are mapped to the arrow keys.
        // The value is in the range -1 to 1
        float translation = Input.GetAxis("Vertical") * speed;
        //float rotation = Input.GetAxis("Horizontal") * rotationSpeed;

        // Make it move 10 meters per second instead of 10 meters per frame...
        translation *= Time.deltaTime;
        //rotation *= Time.deltaTime;

        _energy -= Mathf.Abs(translation * energyDepletionRate);
        //_energy -= Mathf.Abs((rotation * energyDepletionRate) / 10);

        // Move translation along the object's z-axis
        transform.Translate(0, translation, 0);

        // Rotate around our y-axis
        //transform.Rotate(0, 0, -rotation);

        energyText.text = Mathf.CeilToInt(_energy).ToString();
    }

    private IEnumerator MoveTank()
    {
        float elapsedTime = 0f;
        while (elapsedTime < transitionTime)
        {
            elapsedTime += Time.deltaTime;
            float linearT = elapsedTime / transitionTime;
            var newLocation = Math.Interp(new Coords(transform.position), _fuelLocation, linearT);
            transform.position = newLocation.ToVector();
            yield return null;
        }
    }
}