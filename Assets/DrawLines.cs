using UnityEngine;

public class DrawLines : MonoBehaviour
{
    private readonly Coords _point = new Coords(10, 20, -1);
    private readonly Coords origin = new Coords(0, 0,-1);

    private Coords startPointY = new Coords(0, 100, -1);
    private Coords endPointY = new Coords(0, -100, -1);

    private Coords startPointX = new Coords(160, 0, -1);
    private Coords endPointX = new Coords(-160, -0, -1);

    private void Start()
    {
        Coords.DrawLine(startPointY, endPointY, 0.8f, Color.green);
        Coords.DrawLine(startPointX, endPointX, 0.8f, Color.red);
    }
}