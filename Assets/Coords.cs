using UnityEngine;

public class Coords
{
    public float X { get; set; }
    public float Y { get; set; }
    public float Z { get; set; }

    public Coords(float x, float y, float z)
    {
        X = x;
        Y = y;
        Z = z;
    }
    public Coords(Vector3 vector)
    {
        X = vector.x;
        Y = vector.y;
        Z = vector.z;
    }

    public override string ToString()
    {
        return "(" + X + "," + Y + "," + Z + ")";
    }
    public static void DrawPoint(Coords position, float width, Color color)
    {
        GameObject line = new GameObject("Point_" + position);
        LineRenderer lineRenderer = line.AddComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Unlit/Color"));
        lineRenderer.material.color = color;
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, new Vector3(position.X - width / 3.0f, position.Y - width / 3.0f, position.Z));
        lineRenderer.SetPosition(1, new Vector3(position.X + width / 3.0f, position.Y + width / 3.0f, position.Z));
        lineRenderer.startWidth = width;
        lineRenderer.endWidth = width;
    }

    public static void DrawLine(Coords startPoint, Coords endPoint, float width, Color color)
    {
        GameObject line = new GameObject("Line");
        LineRenderer lineRenderer = line.AddComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Unlit/Color"));
        lineRenderer.material.color = color;
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, new Vector3(startPoint.X, startPoint.Y, startPoint.Z));
        lineRenderer.SetPosition(1, new Vector3(endPoint.X, endPoint.Y, endPoint.Z));
        lineRenderer.startWidth = width;
        lineRenderer.endWidth = width;
    }

    public Vector3 ToVector()
    {
        return new Vector3(X, Y, Z);
    }
}