using System;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;

public class Math : MonoBehaviour
{
    public static Coords GetNormal(Coords coords)
    {
        float length = Distance(new Coords(0, 0, 0), coords);

        coords.X /= length;
        coords.Y /= length;
        coords.Z /= length;

        return coords;
    }

    static public float Angle(Coords vector1, Coords vector2)
    {
        float distances = Distance(new Coords(0, 0, 0), vector1) * Distance(new Coords(0, 0, 0), vector2);
        float dotDivided = Dot(vector1, vector2) / distances;

        return Mathf.Acos(dotDivided); // Radians. Degrees = *180/MathF.Pi;
    }

    static public float Dot(Coords vector1, Coords vector2)
    {
        return (vector1.X * vector2.X) + (vector1.Y * vector2.Y) + (vector1.Z * vector2.Z);
    }

    public static float Distance(Coords point1, Coords point2)
    {
        float diffSquared = Pow2(point1.X - point2.X) + Pow2(point1.Y - point2.Y) + Pow2(point1.Z - point2.Z);
        return MathF.Sqrt(diffSquared);
    }

    public static Coords RotateAngle(Coords vector, float angle, bool clockwise) // angle in radians
    {
        if (clockwise)
        {
            angle = 2 * Mathf.PI - angle;
        }
        float xVal = vector.X * Mathf.Cos(angle) - vector.Y * Mathf.Sin(angle);
        float yVal = vector.X * Mathf.Sin(angle) + vector.Y * Mathf.Cos(angle); 

        return new Coords(xVal, yVal, 0);
    }

    public static Coords CrossProduct(Coords vector1, Coords vector2)
    {
        var v = (vector1.Y * vector2.Z) - (vector1.Z * vector2.Y);
        var x = (vector1.Z * vector2.X) - (vector1.X * vector2.Z);
        var w = (vector1.X * vector2.Y) - (vector1.Y * vector2.X);

        return new Coords(v, x, w);
    }

    public static Coords Interp(Coords point1, Coords point2, float linearT)
    {
        var distanceCalcX = point1.X + (point2.X - point1.X) * linearT;
        var distanceCalcY = point1.Y + (point2.Y - point1.Y) * linearT;
        var distanceCalcZ = point1.Z + (point2.Z - point1.Z) * linearT;

        return new Coords(distanceCalcX, distanceCalcY, distanceCalcZ);
    }

    public static Coords LookAt(Coords forwardVector, Coords currentPos, Coords target)
    {
        Coords direction = (new Coords(target.ToVector() - currentPos.ToVector()));
        float angle = Angle(forwardVector, direction);
        bool clockwise = CrossProduct(forwardVector, direction).Z < 0;
        return RotateAngle(forwardVector, angle, clockwise);
    }


    public static float Pow2(float value)
    {
        return value * value;
    }

    public static Coords RotateByAngle(float angleDegrees, Coords forwardVector)
    {
        var angleRadians = angleDegrees * MathF.PI / 180;
        return RotateAngle(forwardVector, angleRadians, false);

    }
}